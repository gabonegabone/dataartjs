CREATE TABLE article
(
    id        SERIAL PRIMARY KEY,
    head      VARCHAR(255),
    body      BYTEA,
    post_date DATE
);

CREATE TABLE article_themed
(
    id        SERIAL PRIMARY KEY,
    head      VARCHAR(255),
    theme     VARCHAR(255),
    body      BYTEA,
    post_date DATE
);