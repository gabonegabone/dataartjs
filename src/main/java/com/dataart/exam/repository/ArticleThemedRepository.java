package com.dataart.exam.repository;

import com.dataart.exam.entity.ArticleThemed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleThemedRepository extends JpaRepository<ArticleThemed, Long> {

    @Query(
            "select article from ArticleThemed article where article.theme = (:theme)"
    )
    List<ArticleThemed> findByTheme(String theme);
}
