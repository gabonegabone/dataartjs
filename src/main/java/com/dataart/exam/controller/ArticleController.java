package com.dataart.exam.controller;

import com.dataart.exam.dto.ArticleDto;
import com.dataart.exam.dto.ArticleThemedDto;
import com.dataart.exam.entity.Article;
import com.dataart.exam.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping // оставить хуйню??
@RequiredArgsConstructor
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping
    public List<? extends ArticleDto> getList(@RequestParam Integer page, @RequestParam Integer size) {
        return articleService.getList(page, size);
    }

    @GetMapping("/{theme}")
    public List<? extends ArticleDto> getWithThemeList(@PathVariable String theme) {
        return articleService.getWithThemeList(theme);
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        return articleService.upload(file);
    }

    @PostMapping("/upload/{theme}")
    public String uploadWithTheme(@PathVariable("theme") String theme, @RequestParam("file") MultipartFile file) {
        return articleService.uploadThemed(theme, file);
    }

}
