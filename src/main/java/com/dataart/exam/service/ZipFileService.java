package com.dataart.exam.service;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Service
public class ZipFileService {

    public String parseZip(MultipartFile file) throws IOException {
        if (!file.getOriginalFilename().endsWith(".zip")) {
            throw new RuntimeException("Uploaded file is not .zip archive");
        }

        File zip = File.createTempFile(UUID.randomUUID().toString(), "tmp");
        FileOutputStream fos = new FileOutputStream(zip);
        IOUtils.copy(file.getInputStream(), fos);
        fos.close();

        ZipFile zipFile = new ZipFile(zip);
        ZipEntry articleEntry = null;
        for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            if (!entry.getName().equals("article.txt")) {
                throw new RuntimeException("Archive contains unexpected files: " + entry.getName());
            } else {
                articleEntry = entry;
            }
        }

        if (articleEntry == null) {
            throw new RuntimeException("Archive does not contain article.txt file");
        }

        return this.getText(zipFile.getInputStream(articleEntry));
    }

    public String getText(InputStream stream) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        try {
            for (int length; (length = stream.read(buffer)) != -1; ) {
                result.write(buffer, 0, length);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return result.toString(StandardCharsets.UTF_8);
    }
}
