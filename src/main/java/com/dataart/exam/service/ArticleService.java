package com.dataart.exam.service;

import com.dataart.exam.dto.ArticleDto;
import com.dataart.exam.dto.ArticleThemedDto;
import com.dataart.exam.entity.Article;
import com.dataart.exam.entity.ArticleThemed;
import com.dataart.exam.mapper.ArticleMapper;
import com.dataart.exam.mapper.ArticleThemedMapper;
import com.dataart.exam.repository.ArticleRepository;
import com.dataart.exam.repository.ArticleThemedRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final ArticleThemedRepository articleThemedRepository;
    private final ArticleMapper articleMapper;
    private final ArticleThemedMapper articleThemedMapper;
    private final ZipFileService zipFileService;

    public List<? extends ArticleDto> getList(Integer page, Integer size) {
        if (page == null) {
            page = 0;
        }
        if (size == null) {
            size = 3;
        }
        Pageable pageable = PageRequest.of(page, size);
        return articleRepository.findAll(pageable)
                .stream().map(
                        x -> {
                        if (x instanceof ArticleThemed) {
                            return articleThemedMapper.parseToDto( (ArticleThemed) x);
                        }
                        return articleMapper.parseToDto(x);
                })
                .collect(Collectors.toList());
    }

    public List<ArticleThemedDto> getWithThemeList(String theme) {
        return articleThemedRepository.findByTheme(theme).stream()
                .map(articleThemedMapper::parseToDto)
                .collect(Collectors.toList());
    }

    public String upload(MultipartFile file) {
        var article = (Article) this.parseToArticle(file);
        articleRepository.save(article);

        return "Your file has been uploaded successfully!";
    }

    public String uploadThemed(String theme, MultipartFile file) {
        var article = (ArticleThemed) this.parseToArticle(file);
        article.setTheme(theme);

        articleThemedRepository.save(article);

        return "Your file has been uploaded successfully!";
    }

    private Article parseToArticle(MultipartFile file) {
        String parsedFile;

        try {
            parsedFile = zipFileService.parseZip(file);
        } catch (IOException e) {
            throw new RuntimeException("Parsing error");
        }

        String[] wholeText = parsedFile.split("\n", 2);
        String head = wholeText[0];
        String body = wholeText[1];

        if (body.equals("")) {
            ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Your article has no text");
        }

        Article article = new ArticleThemed();
        article.setHead(head);
        article.setBody(body.getBytes());
        article.setPostDate(LocalDate.now());

        return article;
    }


}
