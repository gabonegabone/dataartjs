package com.dataart.exam.mapper;

import com.dataart.exam.dto.ArticleThemedDto;
import com.dataart.exam.entity.ArticleThemed;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ArticleThemedMapper {

    ArticleThemedDto parseToDto(ArticleThemed article);
    ArticleThemed parseToEntity(ArticleThemedDto articleThemedDto);

    default byte[] parseToString(String string) {
        return string.getBytes();
    }

    default String parseBytes(byte[] bytes) {
        return new String(bytes);
    }
}
