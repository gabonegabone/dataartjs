package com.dataart.exam.mapper;

import com.dataart.exam.dto.ArticleDto;
import com.dataart.exam.entity.Article;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ArticleMapper {

    ArticleDto parseToDto(Article article);
    Article parseToEntity(ArticleDto articleDto);

    default byte[] parseToString(String string) {
        return string.getBytes();
    }

    default String parseBytes(byte[] bytes) {
        return new String(bytes);
    }

    /*default <T extends Article> T parseFromFile(MultipartFile file, Class<T> requiredType) throws IOException {

        String[] wholeText = new String(file.getBytes()).split("\n", 2);

        String head = wholeText[0];
        String body = wholeText[1];

        article.setHead(head);
        article.setBody(body.getBytes());
        article.setPostDate(LocalDate.now());
    }*/
}
