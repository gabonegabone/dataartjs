package com.dataart.exam.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ArticleDto {

    private String head;
    private String body;
    private LocalDate postDate;
}
