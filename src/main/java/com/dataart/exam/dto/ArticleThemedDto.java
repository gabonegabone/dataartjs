package com.dataart.exam.dto;

import lombok.Data;

@Data
public class ArticleThemedDto extends ArticleDto {

    private String theme;
}
